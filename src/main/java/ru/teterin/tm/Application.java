package ru.teterin.tm;

import ru.teterin.tm.context.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        new Bootstrap().init();
    }

}
