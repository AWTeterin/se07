package ru.teterin.tm.api.context;

import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.api.service.IUserService;

public interface IServiceLocator {

    public IProjectService getProjectService();

    public ITaskService getTaskService();

    public IUserService getUserService();

    public IStateService getStateService();

}
