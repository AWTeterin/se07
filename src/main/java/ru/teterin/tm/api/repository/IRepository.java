package ru.teterin.tm.api.repository;

import ru.teterin.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IRepository<T extends AbstractEntity> {

    public T findOne(
        final String userId,
        final String id
    );

    public Collection<T> findAll(final String userId);

    public T persist(
        final String userId,
        final T t
    );

    public T merge(
        final String userId,
        final T t
    );

    public T remove(
        final String userId,
        final String id
    );

    public void removeAll(final String userId);

}
