package ru.teterin.tm.api.repository;

import ru.teterin.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository extends IRepository<Task> {

    public void removeAllByProjectId(
        final String userId,
        final String projectId
    );

    public Collection<Task> findAllByProjectId(
        final String userId,
        final String projectId
    );

}
