package ru.teterin.tm.api.repository;

import ru.teterin.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    public User findByLogin(final String login);

}
