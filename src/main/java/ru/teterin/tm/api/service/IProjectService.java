package ru.teterin.tm.api.service;

import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;

import java.util.Collection;

public interface IProjectService extends IService<Project> {

    public Project create(
        final String userId,
        final String name,
        final String description,
        final String startDate,
        final String endDate
    );

    public Collection<Task> findAllTaskByProjectId(
        final String userId,
        final String id
    );

}
