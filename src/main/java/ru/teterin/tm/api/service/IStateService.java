package ru.teterin.tm.api.service;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.enumerated.Role;

import java.util.Map;

public interface IStateService {

    public String getLogin();

    public void setLogin(final String login);

    public Role getRole();

    public void setRole(final Role role);

    public String getUserId();

    public void setUserId(final String userId);

    public String getProjectId();

    public void setProjectId(final String projectId);

    public Map<String, AbstractCommand> getCommands();

    public void setCommands(final Map<String, AbstractCommand> commands);

}
