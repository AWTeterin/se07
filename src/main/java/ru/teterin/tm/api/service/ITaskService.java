package ru.teterin.tm.api.service;

import ru.teterin.tm.entity.Task;

public interface ITaskService extends IService<Task> {

    public Task create(
        final String userId,
        final String projectId,
        final String name,
        final String description,
        final String startDate,
        final String endDate
    );

    public Task linkTask(
        final String userId,
        final String projectId,
        final String id
    );

}
