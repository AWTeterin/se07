package ru.teterin.tm.api.service;

import ru.teterin.tm.entity.User;

public interface IUserService extends IService<User> {

    public void createUser(
        final String login,
        final String password
    );

    public User checkUser(
        final String login,
        final String password
    );

    public void changePassword(
        final String login,
        final String oldPassword,
        final String newPassword
    );

    public void editUser(
        final String oldLogin,
        final String newLogin
    );

}
