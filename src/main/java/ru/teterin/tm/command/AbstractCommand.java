package ru.teterin.tm.command;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.enumerated.Role;

import java.util.Scanner;

public abstract class AbstractCommand {

    protected final Scanner scanner = new Scanner(System.in);

    protected final IServiceLocator serviceLocator;

    public AbstractCommand(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute();

    public abstract boolean secure();

    public Role[] roles() {
        return null;
    }

}
