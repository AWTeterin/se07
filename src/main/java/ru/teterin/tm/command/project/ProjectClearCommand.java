package ru.teterin.tm.command.project;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    private final IProjectService projectService = serviceLocator.getProjectService();

    private final IStateService stateService = serviceLocator.getStateService();

    public ProjectClearCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        projectService.removeAll(stateService.getUserId());
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
