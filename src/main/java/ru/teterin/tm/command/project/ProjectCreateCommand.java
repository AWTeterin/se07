package ru.teterin.tm.command.project;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Project;

public final class ProjectCreateCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    private final IProjectService projectService = serviceLocator.getProjectService();

    public ProjectCreateCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new Project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = scanner.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = scanner.nextLine();
        System.out.println("ENTER START DATE:");
        final String startDate = scanner.nextLine();
        System.out.println("ENTER END DATE:");
        final String endDate = scanner.nextLine();
        final String userId = stateService.getUserId();
        final Project project = projectService.create(userId, name, description, startDate, endDate);
        projectService.persist(userId, project);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
