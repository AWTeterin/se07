package ru.teterin.tm.command.project;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Project;

public final class ProjectEditCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    private final IProjectService projectService = serviceLocator.getProjectService();

    public ProjectEditCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Choose project by id and edit it.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER ID:");
        final String id = scanner.nextLine();
        final String userId = stateService.getUserId();
        final Project project = projectService.findOne(userId, id);
        System.out.println(project);
        System.out.println("ENTER NAME:");
        final String name = scanner.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = scanner.nextLine();
        System.out.println("ENTER START DATE:");
        final String startDate = scanner.nextLine();
        System.out.println("ENTER END DATE:");
        final String endDate = scanner.nextLine();
        final Project result = projectService.create(userId, name, description, startDate, endDate);
        result.setId(project.getId());
        projectService.merge(userId, result);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
