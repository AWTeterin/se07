package ru.teterin.tm.command.project;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;

public final class ProjectListCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    private final IProjectService projectService = serviceLocator.getProjectService();

    public ProjectListCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        final Collection<Project> projects = projectService.findAll(stateService.getUserId());
        DateUuidParseUtil.printCollection(projects);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
