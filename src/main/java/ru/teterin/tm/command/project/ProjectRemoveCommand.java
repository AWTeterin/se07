package ru.teterin.tm.command.project;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;

public final class ProjectRemoveCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    private final IProjectService projectService = serviceLocator.getProjectService();

    public ProjectRemoveCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove project by ID.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER ID:");
        final String userId = stateService.getUserId();
        projectService.remove(userId, scanner.nextLine());
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
