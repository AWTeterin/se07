package ru.teterin.tm.command.project;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;

public final class ProjectTasksCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    private final IProjectService projectService = serviceLocator.getProjectService();

    public ProjectTasksCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-tasks";
    }

    @Override
    public String getDescription() {
        return "Show all tasks for this project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID");
        final String id = scanner.nextLine();
        final String userId = stateService.getUserId();
        final Collection<Task> tasks = projectService.findAllTaskByProjectId(userId, id);
        DateUuidParseUtil.printCollection(tasks);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
