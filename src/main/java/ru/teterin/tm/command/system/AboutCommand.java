package ru.teterin.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    public AboutCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Information about building the app.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Created by: " + Manifests.read("Created-By"));
        System.out.println("Build JDK: " + Manifests.read("Build-Jdk"));
        System.out.println("Build number: " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));
    }

    @Override
    public boolean secure() {
        return false;
    }

}
