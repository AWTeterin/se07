package ru.teterin.tm.command.system;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
