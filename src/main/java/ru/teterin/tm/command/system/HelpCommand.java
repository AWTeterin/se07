package ru.teterin.tm.command.system;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;

import java.util.Map;

public final class HelpCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    public HelpCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        final Map<String, AbstractCommand> commandMap = stateService.getCommands();
        for (final AbstractCommand command : commandMap.values()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    @Override
    public boolean secure() {
        return false;
    }

}
