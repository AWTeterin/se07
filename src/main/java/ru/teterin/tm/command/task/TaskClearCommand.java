package ru.teterin.tm.command.task;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    private final ITaskService taskService = serviceLocator.getTaskService();

    private final IStateService stateService = serviceLocator.getStateService();

    public TaskClearCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        taskService.removeAll(stateService.getUserId());
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
