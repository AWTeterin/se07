package ru.teterin.tm.command.task;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Task;

public final class TaskCreateCommand extends AbstractCommand {

    private final ITaskService taskService = serviceLocator.getTaskService();

    private final IStateService stateService = serviceLocator.getStateService();

    public TaskCreateCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task for project.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = scanner.nextLine();
        System.out.println("ENTER PROJECT ID:");
        final String projectId = scanner.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = scanner.nextLine();
        System.out.println("ENTER START DATE:");
        final String startDate = scanner.nextLine();
        System.out.println("ENTER END DATE:");
        final String endDate = scanner.nextLine();
        final String userId = stateService.getUserId();
        final Task task = taskService.create(userId, projectId, name, description, startDate, endDate);
        taskService.persist(userId, task);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
