package ru.teterin.tm.command.task;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Task;

public final class TaskEditCommand extends AbstractCommand {

    private final ITaskService taskService = serviceLocator.getTaskService();

    private final IStateService stateService = serviceLocator.getStateService();

    public TaskEditCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Choose task by id and edit it.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER ID:");
        final String id = scanner.nextLine();
        final String userId = stateService.getUserId();
        final Task task = taskService.findOne(userId, id);
        System.out.println(task);
        System.out.println("ENTER NAME:");
        final String name = scanner.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = scanner.nextLine();
        System.out.println("ENTER START DATE:");
        final String startDate = scanner.nextLine();
        System.out.println("ENTER END DATE:");
        final String endDate = scanner.nextLine();
        final Task result = taskService.create(userId, name, task.getProjectId(),
            description, startDate, endDate);
        result.setId(task.getId());
        taskService.merge(userId, result);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
