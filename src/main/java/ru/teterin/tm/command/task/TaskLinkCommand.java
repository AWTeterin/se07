package ru.teterin.tm.command.task;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.command.AbstractCommand;

public final class TaskLinkCommand extends AbstractCommand {

    private final ITaskService taskService = serviceLocator.getTaskService();

    private final IStateService stateService = serviceLocator.getStateService();

    public TaskLinkCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-link";
    }

    @Override
    public String getDescription() {
        return "Link an task to a project.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LINK]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = scanner.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = scanner.nextLine();
        taskService.linkTask(stateService.getUserId(), projectId, taskId);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
