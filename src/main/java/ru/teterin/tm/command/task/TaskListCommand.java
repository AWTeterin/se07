package ru.teterin.tm.command.task;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;

public final class TaskListCommand extends AbstractCommand {

    private final ITaskService taskService = serviceLocator.getTaskService();

    private final IStateService stateService = serviceLocator.getStateService();

    public TaskListCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        final Collection<Task> tasks = taskService.findAll(stateService.getUserId());
        DateUuidParseUtil.printCollection(tasks);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
