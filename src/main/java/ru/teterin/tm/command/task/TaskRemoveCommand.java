package ru.teterin.tm.command.task;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.command.AbstractCommand;

public final class TaskRemoveCommand extends AbstractCommand {

    private final ITaskService taskService = serviceLocator.getTaskService();

    private final IStateService stateService = serviceLocator.getStateService();

    public TaskRemoveCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove task by ID.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER ID");
        final String userId = stateService.getUserId();
        taskService.remove(userId, scanner.nextLine());
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
