package ru.teterin.tm.command.user;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.command.AbstractCommand;

public final class UserChangePasswordCommand extends AbstractCommand {

    private final IUserService userService = serviceLocator.getUserService();

    private final IStateService stateService = serviceLocator.getStateService();

    public UserChangePasswordCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-password";
    }

    @Override
    public String getDescription() {
        return "Change user password.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String oldPassword = scanner.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = scanner.nextLine();
        final String login = stateService.getLogin();
        userService.changePassword(login, oldPassword, newPassword);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
