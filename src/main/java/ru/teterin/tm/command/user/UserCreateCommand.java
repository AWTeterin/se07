package ru.teterin.tm.command.user;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.command.AbstractCommand;

public final class UserCreateCommand extends AbstractCommand {

    private final IUserService userService = serviceLocator.getUserService();

    public UserCreateCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-reg";
    }

    @Override
    public String getDescription() {
        return "Registration new user.";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = scanner.nextLine();
        userService.createUser(login, password);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return false;
    }

}
