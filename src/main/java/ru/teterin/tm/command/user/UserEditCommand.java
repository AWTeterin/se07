package ru.teterin.tm.command.user;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.command.AbstractCommand;

public final class UserEditCommand extends AbstractCommand {

    private final IUserService userService = serviceLocator.getUserService();

    private final IStateService stateService = serviceLocator.getStateService();

    public UserEditCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user login.";
    }

    @Override
    public void execute() {
        System.out.println("[USER EDIT]");
        System.out.println("ENTER NEW LOGIN:");
        final String newLogin = scanner.nextLine();
        final String oldLogin = stateService.getLogin();
        userService.editUser(oldLogin, newLogin);
        stateService.setLogin(newLogin);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
