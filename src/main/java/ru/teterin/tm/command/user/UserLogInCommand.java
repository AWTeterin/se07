package ru.teterin.tm.command.user;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.entity.User;

public final class UserLogInCommand extends AbstractCommand {

    private final IUserService userService = serviceLocator.getUserService();

    private final IStateService stateService = serviceLocator.getStateService();

    public UserLogInCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "Log in to the task manager.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = scanner.nextLine();
        final User user = userService.checkUser(login, password);
        stateService.setUserId(user.getId());
        stateService.setLogin(user.getName());
        stateService.setRole(user.getRole());
        System.out.println("HELLO " + user.getName());
    }

    @Override
    public boolean secure() {
        return false;
    }

}
