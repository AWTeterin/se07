package ru.teterin.tm.command.user;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;

public final class UserLogOutCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    public UserLogOutCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Log out to the task manager.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        final String login = stateService.getLogin();
        System.out.println("BYE BYE " + login);
        stateService.setUserId(null);
        stateService.setLogin(null);
        stateService.setRole(null);
        stateService.setProjectId(null);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
