package ru.teterin.tm.command.user;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.enumerated.Role;

public final class UserShowCommand extends AbstractCommand {

    private final IStateService stateService = serviceLocator.getStateService();

    public UserShowCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-show";
    }

    @Override
    public String getDescription() {
        return "Show user data.";
    }

    @Override
    public void execute() {
        final Role role = stateService.getRole();
        System.out.println("USER: " + stateService.getLogin());
        System.out.println("ROLE: " + role.displayName());
    }

    @Override
    public boolean secure() {
        return true;
    }

}
