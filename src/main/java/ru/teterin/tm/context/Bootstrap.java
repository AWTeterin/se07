package ru.teterin.tm.context;

import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.command.project.*;
import ru.teterin.tm.command.system.*;
import ru.teterin.tm.command.task.*;
import ru.teterin.tm.command.user.*;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.*;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.service.ProjectService;
import ru.teterin.tm.service.StateService;
import ru.teterin.tm.service.TaskService;
import ru.teterin.tm.service.UserService;
import ru.teterin.tm.util.HashUtil;

import java.util.*;

public class Bootstrap implements IServiceLocator {

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);

    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final StateService stateService = new StateService();

    public void init() {
        usersInit();
        commandsInit();
        final Map<String, AbstractCommand> commandMap = getStateService().getCommands();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            final String cmd = scanner.nextLine();
            final AbstractCommand command = commandMap.get(cmd);
            try {
                if (command == null || (command.secure() && stateService.getRole() == null)) {
                    System.out.println("THE COMMAND IS NOT AVAILABLE!");
                    continue;
                }
                command.execute();
            } catch (ObjectExistException e) {
                System.out.println("OBJECT ALREADY EXIST !");
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            } catch (ObjectNotFoundException e) {
                System.out.println("OBJECT NOT FOUND !");
            }
        }
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public StateService getStateService() {
        return stateService;
    }

    public void usersInit() {
        final User user = new User();
        user.setName("user");
        user.setPassword(HashUtil.md5Custom("user"));
        user.setRole(Role.USER);
        final User admin = new User();
        admin.setName("admin");
        admin.setPassword(HashUtil.md5Custom("admin"));
        admin.setRole(Role.ADMIN);
        userRepository.persist(user.getId(), user);
        userRepository.persist(admin.getId(), admin);
    }

    public void commandsInit() {
        final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();
        final AbstractCommand[] commands = new AbstractCommand[]{
            new ExitCommand(this), new HelpCommand(this), new AboutCommand(this),
            new ProjectListCommand(this), new ProjectTasksCommand(this), new ProjectCreateCommand(this),
            new ProjectEditCommand(this), new ProjectRemoveCommand(this), new ProjectClearCommand(this),
            new TaskListCommand(this), new TaskCreateCommand(this), new TaskEditCommand(this),
            new TaskLinkCommand(this), new TaskRemoveCommand(this), new TaskClearCommand(this),
            new UserCreateCommand(this), new UserEditCommand(this), new UserChangePasswordCommand(this),
            new UserLogInCommand(this), new UserShowCommand(this), new UserLogOutCommand(this)
        };
        for (final AbstractCommand command : commands) {
            commandMap.put(command.getName(), command);
        }
        stateService.setCommands(commandMap);
    }

}
