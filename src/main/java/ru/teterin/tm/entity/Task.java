package ru.teterin.tm.entity;

import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Date;

public final class Task extends AbstractEntity {

    private String projectId;

    private String description;

    private Date dateStart = new Date();

    private Date dateEnd = new Date();

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(final Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return "Task{" +
            "id='" + id + '\'' +
            ", projectId='" + projectId + '\'' +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", dateStart=" + DateUuidParseUtil.DATE_FORMAT.format(dateStart) +
            ", dateEnd=" + DateUuidParseUtil.DATE_FORMAT.format(dateEnd) +
            '}';
    }

}
