package ru.teterin.tm.entity;

import ru.teterin.tm.enumerated.Role;

public final class User extends AbstractEntity {

    private String password;

    private Role role = Role.USER;

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

}
