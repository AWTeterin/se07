package ru.teterin.tm.enumerated;

public enum Role {

    USER("USER"),
    ADMIN("ADMINISTRATOR");

    private final String name;

    Role(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }

}
