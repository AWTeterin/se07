package ru.teterin.tm.exception;

public final class ObjectExistException extends RuntimeException {

    @Override
    public void printStackTrace() {
        System.out.println("Object exist!");
    }

}
