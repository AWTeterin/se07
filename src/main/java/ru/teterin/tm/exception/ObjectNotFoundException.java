package ru.teterin.tm.exception;

public final class ObjectNotFoundException extends RuntimeException {

    @Override
    public void printStackTrace() {
        System.out.println("Object not found!");
    }

}
