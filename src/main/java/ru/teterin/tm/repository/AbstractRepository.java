package ru.teterin.tm.repository;

import ru.teterin.tm.api.repository.IRepository;
import ru.teterin.tm.entity.AbstractEntity;
import ru.teterin.tm.exception.ObjectExistException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public T findOne(
        final String userId,
        final String id
    ) {
        final T t = entities.get(id);
        if (t == null || !t.getUserId().equals(userId)) {
            return null;
        }
        return t;
    }

    @Override
    public Collection<T> findAll(final String userId) {
        final Collection<T> result = new LinkedHashSet<>();
        for (final T t : entities.values()) {
            final String tUserId = t.getUserId();
            if (tUserId.equals(userId)) {
                result.add(t);
            }
        }
        return result;
    }

    @Override
    public T persist(
        final String userId,
        final T t
    ) {
        final boolean hasT = findOne(userId, t.getId()) != null;
        if (hasT) {
            throw new ObjectExistException();
        }
        return entities.put(t.getId(), t);
    }

    @Override
    public T merge(
        final String userId,
        final T t
    ) {
        return entities.put(t.getId(), t);
    }

    @Override
    public T remove(
        final String userId,
        final String id
    ) {
        final T t = findOne(userId, id);
        if (t == null) {
            return null;
        }
        return entities.remove(id);
    }

    @Override
    public void removeAll(final String userId) {
        final Collection<T> entitiesById = findAll(userId);
        for (final T t : entitiesById) {
            entities.remove(t.getId());
        }
    }

}
