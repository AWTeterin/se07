package ru.teterin.tm.repository;

import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedHashSet;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAllByProjectId(
        final String userId,
        final String projectId
    ) {
        final Collection<Task> tasks = findAll(userId);
        for (final Task task : tasks) {
            final String taskProjectId = task.getProjectId();
            if (taskProjectId.equals(projectId)) {
                entities.remove(task.getId());
            }
        }
    }

    @Override
    public Collection<Task> findAllByProjectId(
        final String userId,
        final String projectId
    ) {
        final Collection<Task> result = new LinkedHashSet<>();
        for (final Task task : findAll(userId)) {
            final String taskProjectId = task.getProjectId();
            if (taskProjectId.equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

}
