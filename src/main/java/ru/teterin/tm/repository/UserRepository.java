package ru.teterin.tm.repository;

import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.entity.User;

import java.util.Collection;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        final Collection<User> users = entities.values();
        for (final User user : users) {
            final String userLogin = user.getName();
            if (userLogin.equals(login)) {
                return user;
            }
        }
        return null;
    }

}
