package ru.teterin.tm.service;

import ru.teterin.tm.api.repository.IRepository;
import ru.teterin.tm.api.service.IService;
import ru.teterin.tm.entity.AbstractEntity;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    protected static final String EMPTY_ID = "ID can't be empty!";

    protected static final String EMPTY_DATE = "Date can't be empty!";

    protected static final String EMPTY_OBJECT = "Object or its fields  cannot be empty!";

    protected static final String ERROR_OBJECT_ACCESS = "Object cannot be accessed!";

    private final IRepository<T> repository;

    public AbstractService(final IRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public T findOne(
        final String userId,
        final String id
    ) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        final T t = repository.findOne(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(id));
        if (t == null) {
            throw new ObjectNotFoundException();
        }
        return t;
    }

    @Override
    public Collection<T> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        return repository.findAll(DateUuidParseUtil.toUUID(userId));
    }

    @Override
    public T persist(final String userId, final T t) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        if (t == null || t.getName().isEmpty() || t.getUserId() == null) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        if (!userId.equals(t.getUserId())) {
            throw new IllegalArgumentException(ERROR_OBJECT_ACCESS);
        }
        return repository.persist(DateUuidParseUtil.toUUID(userId), t);
    }

    @Override
    public T merge(
        final String userId,
        final T t
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        if (t == null || t.getName().isEmpty() || t.getUserId() == null) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        if (!userId.equals(t.getUserId())) {
            throw new IllegalArgumentException(ERROR_OBJECT_ACCESS);
        }
        return repository.merge(DateUuidParseUtil.toUUID(userId), t);
    }

    @Override
    public T remove(
        final String userId,
        final String id
    ) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        final T t = repository.remove(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(id));
        if (t == null) {
            throw new ObjectNotFoundException();
        }
        return t;
    }

    @Override
    public void removeAll(final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        repository.removeAll(DateUuidParseUtil.toUUID(userId));
    }

}
