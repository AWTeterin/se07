package ru.teterin.tm.service;

import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final ITaskRepository taskRepository;

    public ProjectService(
        final IProjectRepository projectRepository,
        final ITaskRepository taskRepository
    ) {
        super(projectRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Project remove(
        final String userId,
        final String id
    ) {
        final Project project = super.remove(userId, id);
        taskRepository.removeAllByProjectId(userId, id);
        return project;
    }

    @Override
    public void removeAll(final String userId) {
        super.removeAll(userId);
        taskRepository.removeAll(userId);
    }

    @Override
    public Project create(
        final String userId,
        final String name,
        final String description,
        final String startDate,
        final String endDate
    ) {
        if (userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        if (description == null) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        final boolean noStartDate = startDate == null || startDate.isEmpty();
        final boolean noEndDate = endDate == null || endDate.isEmpty();
        if (noStartDate || noEndDate) {
            throw new IllegalArgumentException(EMPTY_DATE);
        }
        final Project project = new Project();
        project.setUserId(DateUuidParseUtil.toUUID(userId));
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateUuidParseUtil.toDate(startDate));
        project.setDateEnd(DateUuidParseUtil.toDate(endDate));
        return project;
    }

    @Override
    public Collection<Task> findAllTaskByProjectId(
        final String userId,
        final String id
    ) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        final Project project = findOne(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(id));
        if (project == null) {
            throw new ObjectNotFoundException();
        }
        return taskRepository.findAllByProjectId(userId, id);
    }

}
