package ru.teterin.tm.service;

import ru.teterin.tm.api.service.IStateService;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.enumerated.Role;

import java.util.LinkedHashMap;
import java.util.Map;

public final class StateService implements IStateService {

    private String userId;

    private String projectId;

    private String login;

    private Role role;

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public void setLogin(final String login) {
        this.login = login;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public void setRole(final Role role) {
        this.role = role;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(final String userId) {
        this.userId = userId;
    }

    @Override
    public String getProjectId() {
        return projectId;
    }

    @Override
    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    public void setCommands(final Map<String, AbstractCommand> commands) {
        this.commands = commands;
    }

}
