package ru.teterin.tm.service;

import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.*;
import ru.teterin.tm.util.DateUuidParseUtil;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final IProjectRepository projectRepository;

    public TaskService(
        final ITaskRepository taskRepository,
        final IProjectRepository projectRepository
    ) {
        super(taskRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Task create(
        final String userId,
        final String projectId,
        final String name,
        final String description,
        final String startDate,
        final String endDate
    ) {
        if (userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        if (description == null) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        final boolean noStartDate = startDate == null || startDate.isEmpty();
        final boolean noEndDate = endDate == null || endDate.isEmpty();
        if (noStartDate || noEndDate) {
            throw new IllegalArgumentException(EMPTY_DATE);
        }
        final Task task = new Task();
        task.setUserId(DateUuidParseUtil.toUUID(userId));
        task.setProjectId(DateUuidParseUtil.toUUID(projectId));
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUuidParseUtil.toDate(startDate));
        task.setDateEnd(DateUuidParseUtil.toDate(endDate));
        return task;
    }

    @Override
    public Task linkTask(
        final String userId,
        final String projectId,
        final String id
    ) {
        final boolean noTaskId = id == null || id.isEmpty();
        final boolean noUserId = userId == null || userId.isEmpty();
        final boolean noProjectId = projectId == null || projectId.isEmpty();
        if (noProjectId || noTaskId || noUserId) {
            throw new IllegalArgumentException(EMPTY_ID);
        }
        final Project project = projectRepository.findOne(DateUuidParseUtil.toUUID(userId),
            DateUuidParseUtil.toUUID(projectId));
        final Task task = findOne(userId, DateUuidParseUtil.toUUID(id));
        if (project == null || task == null) {
            throw new ObjectNotFoundException();
        }
        task.setProjectId(projectId);
        return task;
    }

}
