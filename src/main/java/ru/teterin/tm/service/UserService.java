package ru.teterin.tm.service;

import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository repository) {
        super(repository);
        userRepository = repository;
    }

    @Override
    public User checkUser(
        final String login,
        String password
    ) {
        final boolean incorrectLogin = login == null || login.isEmpty();
        final boolean incorrectPassword = password == null || password.isEmpty();
        if (incorrectLogin || incorrectPassword) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        final User user = userRepository.findByLogin(login);
        password = HashUtil.md5Custom(password);
        if (user == null || !(password.equals(user.getPassword()))) {
            throw new ObjectNotFoundException();
        }
        return user;
    }

    @Override
    public void changePassword(
        final String login,
        final String oldPassword,
        final String newPassword
    ) {
        final boolean noLogin = login == null || login.isEmpty();
        final boolean noPassword = (oldPassword == null || oldPassword.isEmpty())
            || (newPassword == null || newPassword.isEmpty());
        final User user;
        if (noLogin || noPassword) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        user = checkUser(login, oldPassword);
        user.setPassword(HashUtil.md5Custom(newPassword));
    }

    @Override
    public void createUser(
        final String login,
        final String password
    ) {
        final boolean noLogin = login == null || login.isEmpty();
        final boolean noPassword = password == null || password.isEmpty();
        if (noLogin || noPassword) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        User user = userRepository.findByLogin(login);
        if (user != null) {
            throw new ObjectExistException();
        }
        user = new User();
        user.setUserId(user.getId());
        user.setName(login);
        user.setPassword(HashUtil.md5Custom(password));
        persist(user.getId(), user);
    }

    @Override
    public void editUser(
        final String oldLogin,
        final String newLogin
    ) {

        final boolean noLogin = (oldLogin == null || oldLogin.isEmpty())
            || (newLogin == null || newLogin.isEmpty());
        if (noLogin) {
            throw new IllegalArgumentException(EMPTY_OBJECT);
        }
        if (userRepository.findByLogin(newLogin) != null) {
            throw new ObjectExistException();
        }
        final User user = userRepository.findByLogin(oldLogin);
        user.setName(newLogin);
    }

}
