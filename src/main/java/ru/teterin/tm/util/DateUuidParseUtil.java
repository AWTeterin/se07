package ru.teterin.tm.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class DateUuidParseUtil {

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    public static String toUUID(final String id) {
        try {
            return UUID.fromString(id).toString();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Incorrect format ID! Try this format: " +
                "00000000-0000-0000-0000-000000000000");
        }
    }

    public static Date toDate(final String stringDate) {
        try {
            return DATE_FORMAT.parse(stringDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Incorrect format Date! " +
                "Try this format: 01.01.2020");
        }
    }

    public static void printCollection(final Collection collection) {
        int count = 1;
        for (final Object object : collection) {
            System.out.println(count + ". " + object);
            count++;
        }
    }

}
